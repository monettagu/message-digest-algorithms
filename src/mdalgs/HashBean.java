package mdalgs;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author agustin
 */
public class HashBean {
    public static final String PROP_INPUT = "input";
    public static final String PROP_ALGORITHM = "algorithm";
    public static final String PROP_DIGEST = "digest";

    private String input = "";
    
    private String algorithm = "MD5";
    
    private String digest;
    private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
    private final transient VetoableChangeSupport vetoableChangeSupport = new java.beans.VetoableChangeSupport(this);

    public HashBean() {
    }

    /**
     * @return the input
     */
    public String getInput() {
        return input;
    }

    /**
     * @return the algorithm
     */
    public String getAlgorithm() {
        return algorithm;
    }

    /**
     * @return the digest
     */
    public String getDigest() {
        return digest;
    }

    /**
     * @param input the input to set
     * @throws java.beans.PropertyVetoException
     */
    public void setInput(String input) throws PropertyVetoException {
        java.lang.String oldInput = this.input;
        vetoableChangeSupport.fireVetoableChange(PROP_INPUT, oldInput, input);
        this.input = input;
        propertyChangeSupport.firePropertyChange(PROP_INPUT, oldInput, input);
        this.setDigest(this.digest());
    }

    /**
     * @param algorithm the algorithm to set
     * @throws java.beans.PropertyVetoException
     * @throws java.security.NoSuchAlgorithmException
     */
    public void setAlgorithm(String algorithm) throws PropertyVetoException, NoSuchAlgorithmException {
        java.lang.String oldAlgorithm = this.algorithm;
        vetoableChangeSupport.fireVetoableChange(PROP_ALGORITHM, oldAlgorithm, algorithm);
        MessageDigest md = MessageDigest.getInstance(algorithm);
        this.algorithm = algorithm;        
        propertyChangeSupport.firePropertyChange(PROP_ALGORITHM, oldAlgorithm, algorithm);
        this.setDigest(this.digest());
    }

    /**
     * @param digest the digest to set
     * @throws java.beans.PropertyVetoException
     */
    public void setDigest(String digest) throws PropertyVetoException {
        String oldDigest = this.digest;
        vetoableChangeSupport.fireVetoableChange(PROP_DIGEST, oldDigest, digest);
        this.digest = digest;
        propertyChangeSupport.firePropertyChange(PROP_DIGEST, oldDigest, digest);
    }
    
    /**
     * @param digest the digest to set
     * @throws java.beans.PropertyVetoException
     */
    public void setDigest(byte[] digest) throws PropertyVetoException {
        StringBuilder sb = new StringBuilder();
        for (byte _byte : digest) {
            sb.append(Byte.toString(_byte));
        }
        this.setDigest(sb.toString());
    }
    
    public void addPropertyChangeListener (PropertyChangeListener listener) {
        this.propertyChangeSupport.addPropertyChangeListener(listener);
        
    }
    
    public void removePropertyChangeListener (PropertyChangeListener listener) {
        this.propertyChangeSupport.removePropertyChangeListener(listener);
    }
    
    public void addPropertyChangeListener (String propertyName, PropertyChangeListener listener) {
        this.propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
        
    }
    
    public void removePropertyChangeListener (String propertyName, PropertyChangeListener listener) {
        this.propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
    }
    
    public void addVetoableChangeListener (VetoableChangeListener listener) {
        this.vetoableChangeSupport.addVetoableChangeListener(listener);
        
    }
    
    public void removeVetoableChangeListener (VetoableChangeListener listener) {
        this.vetoableChangeSupport.removeVetoableChangeListener(listener);
    }
    
    public void addVetoableChangeListener (String propertyName, VetoableChangeListener listener) {
        this.vetoableChangeSupport.addVetoableChangeListener(propertyName, listener);
    }
    
    public void removeVetoableChangeListener (String propertyName, VetoableChangeListener listener) {
        this.vetoableChangeSupport.removeVetoableChangeListener(propertyName, listener);
    }
    
    /**
     * Calculate message digest.
     * @return message digest for input in hex format.
     */
    private String digest() {
        try {
            MessageDigest md = MessageDigest.getInstance(this.getAlgorithm());
            try {
                return new BigInteger(1,md.digest(this.input.getBytes("UTF-8"))).toString(16);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(HashBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(HashBean.class.getName()).log(Level.SEVERE, "Could not get digest algorithm.", ex);
        }
        return "";
    }
}
